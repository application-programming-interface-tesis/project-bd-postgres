FROM postgres:15

RUN apt-get update && apt-get install -y \
    procps \
    net-tools \
    iproute2 \
    nano \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

#COPY ./pg_hba.conf /var/lib/postgresql/data/pg_hba.conf
 
EXPOSE 5432
