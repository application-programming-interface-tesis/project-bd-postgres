--CREATE DATABASE data_bd;
--\c data_bd;

--docker exec -it bd-postgres bash
--psql -U postgres data_bd
--psql -U data_integration data_bd

-- CREACION DE USUARIO GENERAL   
CREATE USER data_integration WITH PASSWORD '44JLhpZu6MJrDKGZ4';

-- CREACION DE ESQUEMA
CREATE SCHEMA etl;

-- CREACION DE PERMISOS
GRANT USAGE ON SCHEMA etl TO data_integration;
GRANT CREATE ON SCHEMA etl TO data_integration;

-- Establecer el search_path para el esquema 'etl'
ALTER USER data_integration SET search_path TO etl, public;

-- CREACION DE SEGUNDO ESQUEMA
CREATE SCHEMA tstorage;

-- CREACION DE PERMISOS
GRANT USAGE ON SCHEMA tstorage TO data_integration;
GRANT CREATE ON SCHEMA tstorage TO data_integration;

-- Establecer el search_path para el esquema 'tstorage'
ALTER USER data_integration SET search_path TO tstorage, public;

-- Conceder privilegios en las tablas del esquema 'public'
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO data_integration;

--CREACION DE TABLAS EN EL ESQUEMA "etl"

CREATE TABLE etl.dat_catalogo_servicios
(
    fecha_proceso date,
	producto character varying(200),
    segmento_producto character varying(200),
    id_tipo_plan character varying(200),
    descripcion character varying(200),
    codigo_one character varying(200),
    id_plan character varying(200),
    nombre_plan character varying(200),
    tarifa_basica numeric(12,3),
    fecha_oferta date,
    tipo_componentes character varying(200),
    uso_humano character varying(200),
    cuadernillo character varying(200),
    llamadas_ilimitadas character varying(200),
    clasificacion_amx character varying(200),
    oferta_vigente character varying(200),
    categoria_mkt character varying(200),
    plataforma_de_autogestion_m2m character varying(200),
    soluciones_de_iot character varying(200),
    reporte_iot_amx character varying(200),
    seg_corpo_2 character varying(200),
    ingresos_por_servicios character varying(200),
    mercado character varying(200),
    forma_de_pago character varying(200),
    producto_especifico_2 character varying(200),
    categoria character varying(200),
    tipo_de_consumo character varying(200),
    tarifa_de_voz numeric(12,3),
    comercial character varying(200),
    llamadas_ilimitadas_offnet character varying(200),
    llamadas_ilimitadas_onnet character varying(200),
    llamadas_ilimitadas_a_5_numeros_favoritos_claro character varying(200),
    minutos_incluidos_ldi character varying(200),
    minutos_con_promocion character varying(200),
    sms character varying(200),
    megas_incluidos character varying(200),
    megas_promocion character varying(200),
    facebook character varying(200),
    redes_sociales character varying(200),
    whatsapp character varying(200),
    whatsapp_roaming character varying(200),
    precio_minuto_onnet character varying(200),
    precio_minuto_movistar character varying(200),
    precio_minuto_cnt character varying(200),
    telecsa character varying(200),
    precio_minuto_fijas character varying(200)  
);

GRANT ALL ON TABLE etl.dat_catalogo_servicios TO data_integration;

----

CREATE TABLE etl.dat_clientes_promocion
(
    fecha_proceso date,
    identificacion character varying(25),
    detalle character varying(200),
    categoria character varying(200),
    promo character varying(200),
    cupo character varying(200),
    calificacion character varying(200)
);

GRANT ALL ON TABLE etl.dat_clientes_promocion TO data_integration;
-----

CREATE TABLE etl.dat_ofertas_hogar
(
    fecha_proceso date,
    familia_producto character varying(200),
    producto_general character varying(200),
    producto_especifico character varying(200),
    mercado character varying(200),
    producto character varying(200),
    segmento_producto character varying(200),
    forma_de_pago character varying(200),
    producto_especifico_2 character varying(200),
    categoria character varying(200),
    tipo_de_consumo character varying(200),
    uso_humano character varying(200),
    cuadernillo character varying(200),
    comercial character varying(200),
    oferta_vigente character varying(200),
    clasificacion_amx character varying(200),
    categoria_mkt character varying(200),
    tipo_componentes character varying(200),
    fecha_oferta date,
    codigo_one character varying(200),
    id_plan character varying(200),
    nombre_plan character varying(200),
    tarifa_basica character varying(200),
    tarifa_de_voz character varying(200),
    id_tipo_plan character varying(200),
    descripcion character varying(200),
    minutos_incluidos_local character varying(200),
    minutos_incluidos_ldi character varying(200),
    minutos_con_promocion character varying(200),
    sms character varying(200),
    llamadas_ilimitadas character varying(200),
    megas_incluidos character varying(200),
    megas_promocion character varying(200),
    facebook character varying(200),
    redes_sociales character varying(200),
    whatsapp character varying(200),
    whatsapp_roaming character varying(200),
    precio_minuto_onnet character varying(200),
    precio_minuto_movistar character varying(200),
    precio_minuto_cnt character varying(200),
    telecsa character varying(200),
    precio_minuto_fijas character varying(200),
    adendum character varying(200),
    glosa_en_factura character varying(200),
    ticket_de_kanboard character varying(200),
    cod_producto character varying(200),
    ingresos_por_servicios character varying(200)
);

GRANT ALL ON TABLE etl.dat_ofertas_hogar TO data_integration;

-------------

CREATE TABLE etl.dat_plan_servicios
(
    fecha_inicio date,
    fecha_fin date,
    codigo integer,
    id_promocion character varying(200),
    descripcion_promo character varying(200),
    offering_id character varying(200),
    promocion character varying(200),
    clasif_final_cuadernillo character varying(200),
    grupo character varying(200),
    tipo_paquete character varying(200),
    megas_otorgados character varying(200),
    megas_redes character varying(200),
    min_otorgados character varying(200),
    min_ilimitados_5_claro character varying(200),
    price_after_tax numeric(12,2),
    megas_redes_whatsapp character varying(200),
    megas_redes_fbmsn character varying(200),
    ingresos_por_servicio character varying(200)
);

GRANT ALL ON TABLE etl.dat_plan_servicios TO data_integration;




